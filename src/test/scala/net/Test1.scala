package net

import org.scalatest._
import org.scalatest.flatspec.AnyFlatSpec

class Test1 extends AnyFlatSpec with BeforeAndAfterAll with ParallelTestExecution {

  behavior of "foo"

  it should "a - 5 seconds " in {
    Thread.sleep(5000); succeed
  }

  it should "b - 3 seconds" in {
    Thread.sleep(3000); succeed
  }

  it should "c - 1 second" in {
    Thread.sleep(1000); succeed
  }

  override protected def beforeAll() = {
    println("test1: BEFORE now (epoch seconds): " + java.time.Instant.now().getEpochSecond)
  }

  override protected def afterAll() = {
    println("test1: AFTER now (epoch seconds): " + java.time.Instant.now().getEpochSecond)
  }

}

